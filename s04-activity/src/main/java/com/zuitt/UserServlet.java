package com.zuitt;

import java.io.IOException;
import java.io.PrintWriter;

import jakarta.servlet.RequestDispatcher;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

public class UserServlet extends HttpServlet {		

	/**
	 * 	D ko alam pano e fix ung Method not allowed sir, pag e manual ko e type ung site /details gumagana naman
	 */
	private static final long serialVersionUID = 7560556849380195433L;
	public void init() throws ServletException {  
        System.out.println("******************************************");
        System.out.println("UserServlet has been initialized. ");
        System.out.println("******************************************");
    }
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException {
		
	}
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		ServletContext srvContext = getServletContext();
		HttpSession session = req.getSession();	
		String fName = String.valueOf(req.getParameter("firstname"));
		String lName = String.valueOf(req.getParameter("lastname"));
		String email = String.valueOf(req.getParameter("email"));
		String contact = System.getProperty("contact");
		
		
		System.getProperties().put("firstname", fName);		//System Properties
				
	    session.setAttribute("lastname", lName);			//HTTP session
	    srvContext.setAttribute("email",email);				//Servlet Context setAtt
	    //res.sendRedirect("user?contact="+contact);			//redirect
	    RequestDispatcher rd = req.getRequestDispatcher("details");
		rd.forward(req,res);
		
	}
	public void destroy() {        
        System.out.println("******************************************");
        System.out.println("UserServlet has been destroyed. ");
        System.out.println("******************************************");
    }
}
